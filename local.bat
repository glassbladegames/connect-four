rmdir /s /q built
docker build . -f builders/windows/Dockerfile -t connect-four-windows --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
if %errorlevel% neq 0 exit /b %errorlevel%
docker create --name connect-four-windows connect-four-windows
docker cp connect-four-windows:/game/built built
docker rm connect-four-windows
cd built
connect-four.exe -host=localhost
