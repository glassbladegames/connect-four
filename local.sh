#!/bin/sh
set -eux
rm -rf built
docker build . -f builders/ubuntu/Dockerfile -t connect-four-ubuntu --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
docker create --name connect-four-ubuntu connect-four-ubuntu
docker cp connect-four-ubuntu:/game/built built
docker rm connect-four-ubuntu
cd built
./connect-four -host=localhost
