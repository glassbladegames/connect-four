#pragma once
#define MainMenuButton(button)\
{\
	button->bordercolor = {191,191,191,255};\
	button->hoverbackcolor = {127,0,0,255};\
	button->hoverbordercolor = {255,255,255,255};\
	button->hoverable = true;\
}
