#include <slice.h>
#include <slicefps.h>
#include "online.h"
#include "local.h"
#define conn4_port 42069
#include <enet/enet.h>
#include "event-ids.h"
#include "mainmenu.h"
ENetPeer* peer;
struct Player
{
	Uint32 user_id;
	char* name;
	bool invite;
	slBox* namebox;
	slBox* invitebox;
	bool ingame;
	slBU _index_;
	slBox* listbox;
};
slList Players("Players",offsetof(Player,_index_),true);
Player* PlayerByInviteBox (slBox* invitebox)
{
	for (slBU cur = 0; cur < Players.itemcount; cur++)
	{
		Player* player = *(Players.items + cur);
		if (player->invitebox == invitebox) return player;
	};
	return NULL;
};
void OnGameInviteAccept (slBox* invitebox)
{
	// We're accepting someone's invite.
	Player* player = PlayerByInviteBox(invitebox);
	if (!player) return; // This really shouldn't happen. Ever.
	Uint8 buf [5];
	*buf = UserEvent_AcceptInvite;
	*(Uint32*)(buf + 1) = player->user_id;
	ENetPacket* packet = enet_packet_create(buf,5,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(peer,0,packet);
};
void OnInvitePrimaryClick (slBox* invitebox)
{
	// We're inviting another player.
	Player* player = PlayerByInviteBox(invitebox);
	if (!player) return; // This really shouldn't happen. Ever.
	invitebox->SetTexRef(slRenderText("Invite Sent"));
	invitebox->drawmask = {0,63,127,255};
	invitebox->hoverable = false;
	invitebox->onclick = NULL;
	Uint8 buf [5];
	*buf = UserEvent_InvitePlayer;
	*(Uint32*)(buf + 1) = player->user_id;
	ENetPacket* packet = enet_packet_create(buf,5,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(peer,0,packet);
};
//slBox* ListPanel;
char* DefaultName (unsigned int user_id)
{
	char* name;
	asprintf(&name,"User #%u",user_id);
	return name;
};
void NormalInviteBox (slBox* invitebox)
{
	invitebox->SetTexRef(slRenderText("Invite?"));
	invitebox->drawmask = {127,127,0,255};
	invitebox->hoverdrawmask = {255,255,0,255};
	invitebox->hoverable = true;
	invitebox->hoverbackcolor = {63,63,0,255};
	invitebox->hoverbordercolor = {191,191,0,255};
	invitebox->onclick = OnInvitePrimaryClick;
};
bool in_a_game;
void SetLobbyVis (bool vis);
//slBU PlayerListOffset = 0;
scrScroll* PlayerList;
void PlayerListUpdate ()
{
	for (slBU cur = 0; cur < Players.itemcount; cur++)
	{
		Player* player = *(Players.items + cur);
		slBox* container = player->listbox;
		player->namebox->visible = container->visible;
		player->invitebox->visible = container->visible && !player->ingame;
		slSetBoxDims(player->namebox,0,0,0.7,1,40);
		slSetBoxDims(player->invitebox,0.71,0.1,0.28,0.8,40);
		slRelBoxDims(player->namebox,container);
		slRelBoxDims(player->invitebox,container);
	};
};
void CreatePlayer (Uint32 user_id)
{
	Player* out = malloc(sizeof(Player));
	out->user_id = user_id;
	//out->name = NULL;
	out->name = DefaultName(user_id);
	out->invite = false;
	out->namebox = slCreateBox(slRenderText(out->name));
	out->invitebox = slCreateBox();
	NormalInviteBox(out->invitebox);
	out->listbox = slCreateBox();
	Players.Add(out);
	out->ingame = false;
	//SetLobbyVis(!in_a_game);
	PlayerList->AddBox(out->listbox);
};
void Handle_NewPlayer (ENetPacket* packet)
{
	// Another player has joined.
	if (packet->dataLength < 5) return;
	CreatePlayer(*(Uint32*)(packet->data + 1));
};
Player* PlayerByID (Uint32 id)
{
	for (slBU cur = 0; cur < Players.itemcount; cur++)
	{
		Player* player = *(Players.items + cur);
		if (player->user_id == id) return player;
	};
	return NULL;
};
void Handle_SetName (ENetPacket* packet)
{
	// Some player has changed name.
	if (packet->dataLength < 5) return;
	Uint32 id = *(Uint32*)(packet->data + 1);
	Player* player = PlayerByID(id);
	if (!player) return;
	if (player->name) free(player->name);
	size_t len = packet->dataLength - 5;
	if (len)
	{
		player->name = malloc(len + 1);
		*(player->name + len) = '\0';
		memcpy(player->name,packet->data + 5,len);
	}
	else player->name = DefaultName(player->user_id);
	player->namebox->SetTexRef(slRenderText(player->name));
};
void Handle_InvitePlayer (ENetPacket* packet)
{
	// We've been invited by another player.
	if (packet->dataLength < 5) return;
	Player* player = PlayerByID(*(Uint32*)(packet->data + 1));
	if (!player) return;
	player->invitebox->SetTexRef(slRenderText("Invite Received"));
	player->invitebox->drawmask = {0,127,0,255};
	player->invitebox->hoverdrawmask = {0,255,0,255};
	player->invitebox->hoverbackcolor = {0,63,0,255};
	player->invitebox->hoverbordercolor = {0,191,0,255};
	player->invitebox->onclick = OnGameInviteAccept;
};
void DestroyPlayer (Player* player)
{
	if (player->name) free(player->name);
	slDestroyBox(player->namebox);
	slDestroyBox(player->invitebox);
	Players.Remove(player);
	PlayerList->RemoveBox(player->listbox);
	free(player);
};
void Handle_PlayerLeft (ENetPacket* packet)
{
	// Another player left.
	if (packet->dataLength < 5) return;
	Uint32 id = *(Uint32*)(packet->data + 1);
	Player* player = PlayerByID(id);
	if (!player) return;
	DestroyPlayer(player);
};
void Handle_PlayerStatus (ENetPacket* packet)
{
	// A player is or is not in a game.
	if (packet->dataLength < 6) return;
	Uint32 id = *(Uint32*)(packet->data + 1);
	Player* player = PlayerByID(id);
	if (!player) return;
	player->ingame = *(packet->data + 5);
	player->invitebox->visible = !(player->ingame || in_a_game);
	NormalInviteBox(player->invitebox);
	player->namebox->drawmask.a = player->ingame ? 127 : 255;
};
extern slBox* WinTextBox;
extern slBox* DroppingCoin;
extern Uint8* states;
extern slBox** Droppers;
extern bool IsBlue;
extern slList Coins;
extern slScalar DroppingCoinTargetY;
void ResetOnlineGame ()
{
	WinTextBox->visible = false;
	memset(states,Empty,42);
	IsBlue = false;
	slBox* box;
	/*for (Uint8 x = 0; x < 7; x++)
	{
		box = *(Droppers + x);
		box->visible = false;
		box->hovertexref = slLoadTexture(IsBlue ? "c4/coin_blue.png" : "c4/coin_red.png");
	};*/
	Coins.Clear(slDestroyBox);
	DroppingCoin = NULL;
};
slBox* QuitOnlineBox;
slBox* NameChangeButton;
slBox* NCIB_Box;
slBox* separator;
slBox* namebox;
void SetLobbyVis (bool vis)
{
	/*for (slBU cur = 0; cur < Players.itemcount; cur++)
	{
		Player* player = *(Players.items + cur);
		player->namebox->visible = vis;
		player->invitebox->visible = vis && !player->ingame;
	};*/
	PlayerList->SetVisible(vis);
	QuitOnlineBox->visible = vis;
	NameChangeButton->visible = vis;
	if (!vis) NCIB_Box->visible = false;
	namebox->visible = vis;
	separator->visible = vis;
};
void DroppersAway ()
{
	for (Uint8 cur = 0; cur < 7; cur++) (*(Droppers + cur))->visible = false;
};
slBox* StartBox;
void SetGameVis (bool vis)
{
	in_a_game = vis;
	SetBoardVis(vis);
	if (!vis)
	{
		StartBox->visible = false;
		DroppersAway();
	};
};
bool i_am_Blue;
slScalar gametime;
char* Name = NULL;
void Handle_GameStart (ENetPacket* packet)
{
	if (packet->dataLength < 6) return;
	i_am_Blue = *(packet->data + 1);
	Uint32 id = *(Uint32*)(packet->data + 2);
	Player* player = PlayerByID(id);
	if (!player) return;
	char* text;
	asprintf(&text,"You are %s, playing against %s.",i_am_Blue ? "BLUE" : "RED",player->name);
	StartBox->SetTexRef(slRenderText(text));
	StartBox->visible = true;
	StartBox->drawmask.a = 255;
	StartBox->backcolor = {i_am_Blue ? 0 : 255,0,i_am_Blue ? 255 : 0,255};
	free(text);
	gametime = 0;
	SetGameVis(true);
	SetLobbyVis(false);
	for (Uint8 x = 0; x < 7; x++) (*(Droppers + x))->SetHoverTexRef(slLoadTexture(i_am_Blue ? "c4/coin_blue.png" : "c4/coin_red.png"));
};
void Handle_WhoseTurn (ENetPacket* packet)
{
	if (packet->dataLength < 2) return;
	bool IsBlue = *(packet->data + 1);
	bool visi = i_am_Blue == IsBlue;
	for (Uint8 x = 0; x < 7; x++) (*(Droppers + x))->visible = visi;
};
void Handle_PiecePlaced (ENetPacket* packet)
{
	if (packet->dataLength < 4) return;
	Uint8 x = *(packet->data + 1);
	Uint8 y = *(packet->data + 2);
	bool IsBlue = *(packet->data + 3);
	*(states + (y * 7) + x) = IsBlue ? Blue : Red;
	slBox* coin = slCreateBox(slLoadTexture(IsBlue ? "c4/coin_blue.png" : "c4/coin_red.png"));
	slSetBoxDims(coin,0.15 + (x * 0.1),0.1,0.1,0.1,128);
	DroppingCoinTargetY = 0.7 - (y * 0.1);
	DroppingCoin = coin;
	coin->maintainaspect = true;
	Coins.Add(coin);
};
void Handle_GameOver (ENetPacket* packet)
{
	if (packet->dataLength < 2) return;
	WinTextBox->visible = true;
	char* text;
	switch (*(packet->data + 1))
	{
		case GAMEOVER_ABANDON:
		text = "Other player abandoned the match.";
		break;
		case GAMEOVER_BLUEWIN:
		text = "Blue wins!";
		break;
		case GAMEOVER_REDWIN:
		text = "Red wins!";
		break;
		case GAMEOVER_STALEMATE:
		text = "Stalemate - nobody wins.";
		//break;
	};
	WinTextBox->SetTexRef(slRenderText(text));
	DroppersAway();
};
void HandleEvent (enet_uint8 channel, ENetPacket* packet)
{
	//switch (channel)
	//{
	//	case 0:
		if (!packet->dataLength) return;
		switch (*(packet->data))
		{
			case UserEvent_NewPlayer:
			Handle_NewPlayer(packet);
			break;
			case UserEvent_SetName:
			Handle_SetName(packet);
			break;
			case UserEvent_InvitePlayer:
			Handle_InvitePlayer(packet);
			break;
			case UserEvent_PlayerLeft:
			Handle_PlayerLeft(packet);
			break;
			case GameEvent_GameStart:
			Handle_GameStart(packet);
			break;
			case GameEvent_WhoseTurn:
			Handle_WhoseTurn(packet);
			break;
			case GameEvent_PiecePlaced:
			Handle_PiecePlaced(packet);
			break;
			case GameEvent_GameOver:
			Handle_GameOver(packet);
			break;
			case UserEvent_PlayerStatus:
			Handle_PlayerStatus(packet);
			//break;
		};
	//};
};
bool CancelReq;
void OnCancelReq () { CancelReq = true; };
#define ConnResult_Success 0
#define ConnResult_Cancel 1
#define ConnResult_Fail 2
bool QuitOnlineReq;
void OnQuitOnline () { QuitOnlineReq = true; };
slTypingBox* NameChangeInputBox = NULL;
void NotifyNameChange ()
{
	slBU len = slStrLen(Name);
	Uint8* buf = malloc(len + 1);
	*buf = UserEvent_SetName;
	memcpy(buf + 1,Name,len);
	ENetPacket* packet = enet_packet_create(buf,len + 1,ENET_PACKET_FLAG_RELIABLE);
	free(buf);
	enet_peer_send(peer,0,packet);
};
#define LocalDefaultNameText "(using default name)"
void OnNameSubmit ()
{
	if (Name) free(Name);
	Name = NameChangeInputBox->GenText();
    /* Unlike the old version, ^ this will return a zero-length allocated string rather than NULL. */
    if (!*Name)
    {
        free(Name);
        Name = NULL;
    }
	NotifyNameChange();
    NameChangeInputBox->container->xy.y = 10;
	NameChangeInputBox->UpdateCells();
	namebox->SetTexRef(slRenderText(Name ? Name : LocalDefaultNameText));
};
void OnNameChangeBegin ()
{
	NameChangeInputBox->container->xy.y = 0.45;
    NameChangeInputBox->SetText("");
	NameChangeInputBox->Focus();
};
void BackToLobby ()
{
	ResetOnlineGame();
	SetGameVis(false);
	SetLobbyVis(true);
};
void OnOnlineDrop (slBox* dropper)
{
	// Stop checking after 6-1, not 7-1, because 7-1 is logically going to be it.
	Uint8 x;
	for (x = 0; x < 6; x++) if (dropper == *(Droppers + x)) break;
	Uint8 buf [2] = {GameEvent_PiecePlaced,x};
	ENetPacket* packet = enet_packet_create(buf,2,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(peer,0,packet);
};
char* Host;
void DoOnline ()
{
	if (enet_initialize())
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"Could not initialize ENet.","Networking capabilities unavailable.",NULL);
		exit(-1);
	};
	ENetHost* client = enet_host_create(NULL,1,4,0,0);
	ENetAddress addr;
	enet_address_set_host(&addr,Host);
	addr.port = conn4_port;
	peer = enet_host_connect(client,&addr,4,0);
	ENetEvent event;
	slBox* statusbox = slCreateBox(slRenderText("Connecting to lobby server..."));
	slSetBoxDims(statusbox,0,0.57,1,0.06,30);
	slBox* spinner = slCreateBox(slLoadTexture("c4/spinner2.png"));
	slSetBoxDims(spinner,0.45,0.45,0.1,0.1,30);
	slSetBoxRots(spinner,true);
	slBox* cancelbox = slCreateBox(slRenderText("Cancel"));
	slSetBoxDims(cancelbox,0.4,0.65,0.2,0.05,30);
	MainMenuButton(cancelbox);
	CancelReq = false;
	cancelbox->onclick = OnCancelReq;
	Uint8 conn_result; // 0: OK. 1: CANCEL. 2: TIMED-OUT.
	while (true)
	{
		slCycle();
		spinner->rotangle += slGetDelta() * 360;
		if (CancelReq || slGetReqt())
		{
			conn_result = ConnResult_Cancel;
			break;
		};
		while (enet_host_service(client,&event,0) > 0)
		{
			switch (event.type)
			{
				case ENET_EVENT_TYPE_CONNECT:
				//printf("Connection established!\n");
				conn_result = ConnResult_Success;
				goto CONNECT_CONTINUE;
				//break;
				case ENET_EVENT_TYPE_DISCONNECT:
				//printf("Connection not established.\n");
				conn_result = ConnResult_Fail;
				goto CONNECT_CONTINUE;
				//break;
			};
		};
	};
	CONNECT_CONTINUE:
	if (conn_result == ConnResult_Fail)
	{
		statusbox->SetTexRef(slRenderText("Unable to connect.",{255,0,0,255}));
		slScalar alpha = 1;
		slBox* xbox = slCreateBox(slRenderText("X",{255,0,0,255}));
		slSetBoxDims(xbox,0,0,1,1,29);
		slRelBoxDims(xbox,spinner);
		while (!slGetReqt())
		{
			slCycle();
			alpha -= slGetDelta();
			spinner->drawmask.a = slClamp255(alpha);
			if (CancelReq) break;
		};
		slDestroyBox(xbox);
	};
	slDestroyBox(spinner);
	slDestroyBox(statusbox);
	slDestroyBox(cancelbox);
    if (conn_result == ConnResult_Cancel)
    {
		enet_peer_disconnect_now(peer,0);
    }
    else if (conn_result != ConnResult_Fail)
    {

    	/* NOW WE ARE FINALLY CONNECTED! */
    	/* Create the game stuff and make it invisible. */
    	/* When the server switches us into a game, make lobby stuff invisible and game stuff visible. */
    	WinTextBox = slCreateBox();
    	slSetBoxDims(WinTextBox,0,0,1,1,29);
    	WinTextBox->onclick = BackToLobby;
    	WinTextBox->visible = false;
    	WinTextBox->backcolor = {93,93,93,93};
    	WinTextBox->bordercolor = {127,127,127,127};
    	Droppers = malloc(sizeof(slBox*) * 7);
    	Uint8 x,y;
    	CreateBoard();
    	for (x = 0; x < 7; x++)
    	{
    		slBox* dropper = slCreateBox(slLoadTexture("c4/arrow.png"),slLoadTexture("c4/coin_red.png"));
    		slSetBoxDims(dropper,0.15 + (x * 0.1),0.1,0.1,0.1,96);
    		dropper->maintainaspect = true;
    		dropper->hoverable = true;
    		dropper->onclick = OnOnlineDrop;
    		*(Droppers + x) = dropper;
    	};
    	states = calloc(42,1);
    	// No buttons for user to return to lobby or main menu during online match.
    	// We want to discourage abandoning, and rounds are short anyway.
    	// If user really wants to go, he can close the game.
    	StartBox = slCreateBox();
    	slSetBoxDims(StartBox,0,0,1,1,48);
    	SetGameVis(false);
    	/* Lobby UI */
    	slBox* ListPanel = slCreateBox();
    	slSetBoxDims(ListPanel,0.25,0,0.75,1,50);
    	QuitOnlineBox = slCreateBox(slRenderText("Back to Main Menu"));
    	slSetBoxDims(QuitOnlineBox,0.01,0.94,0.23,0.05,30);
    	MainMenuButton(QuitOnlineBox);
    	QuitOnlineBox->onclick = OnQuitOnline;
    	QuitOnlineReq = false;
    	NameChangeButton = slCreateBox(slRenderText("Change Name"));
    	MainMenuButton(NameChangeButton);
    	slSetBoxDims(NameChangeButton,0.01,0.01,0.23,0.05,30);
    	NameChangeButton->onclick = OnNameChangeBegin;
    	NCIB_Box = slCreateBox();
    	NCIB_Box->backcolor = {0,0,0,255};
    	NCIB_Box->bordercolor = {255,255,255,255};
    	slSetBoxDims(NCIB_Box,0.05,10,0.9,0.1,29);
    	NameChangeInputBox = slCreateTypingBox(NCIB_Box);
    	NameChangeInputBox->onenter = OnNameSubmit;
    	NCIB_Box->visible = false;
    	namebox = slCreateBox(slRenderText(LocalDefaultNameText));
    	slSetBoxDims(namebox,0,0,1,0.0625,40);
    	separator = slCreateBox();
    	separator->bordercolor = {47,47,47,255};
    	slSetBoxDims(separator,0,0.0625,1,0,40);
    	slRelBoxDims(separator,ListPanel);
    	slRelBoxDims(namebox,ListPanel);
    	bool GracefulDisconnect = true;
    	ListPanel->xy.y += namebox->wh.h;
    	ListPanel->wh.h -= namebox->wh.h;
    	PlayerList = scrCreateScroll(ListPanel,20);
    	PlayerList->update = PlayerListUpdate;
    	ListPanel->backcolor = {31,31,31,96};
    	namebox->backcolor = {31,31,31,96};
    	while (!slGetReqt())
    	{
    		slCycle();
    		gametime += slGetDelta();
    		if (gametime < 2) StartBox->drawmask.a = 255;
    		else if (gametime < 3) StartBox->drawmask.a = slClamp255(3 - gametime);
    		else StartBox->visible = false;
    		StartBox->backcolor.a = StartBox->drawmask.a;
    		if (DroppingCoin)
    		{
    			DroppingCoin->xy.y += slGetDelta() * 4;
    			if (DroppingCoin->xy.y >= DroppingCoinTargetY)
    			{
    				DroppingCoin->xy.y = DroppingCoinTargetY;
    				DroppingCoin = NULL;
    			};
    		};
    		if (QuitOnlineReq) break;
    		while (enet_host_service(client,&event,0) > 0)
    		{
    			switch (event.type)
    			{
    				case ENET_EVENT_TYPE_DISCONNECT:
    				//printf("Disconnected during cycling!\n");
    				//goto CLEANUP;
    				GracefulDisconnect = false;
    				goto CONNLOST;
    				case ENET_EVENT_TYPE_RECEIVE:
    				HandleEvent(event.channelID,event.packet);
    				enet_packet_destroy(event.packet);
    				//printf("Received packet.\n");
    				//break;
    			};
    		};
    	};
    	CONNLOST:
    	/* Cleanup Game Stuff */
    	slDestroyBox(WinTextBox);
    	for (x = 0; x < 7; x++) slDestroyBox(*(Droppers + x));
    	free(Droppers);
    	DestroyBoard();
    	Coins.Clear(slDestroyBox);
    	DroppingCoin = NULL;
    	free(states);
    	slDestroyBox(StartBox);
    	/* Cleanup Lobby Stuff */
    	slDestroyBox(NameChangeButton);
    	slDestroyBox(QuitOnlineBox);
    	slDestroyBox(separator);
    	slDestroyBox(namebox);
    	Players.UntilEmpty(DestroyPlayer);
    	scrDestroyScroll(PlayerList);
    	slDestroyTypingBox(NameChangeInputBox);
    	if (GracefulDisconnect)
    	//{
    		enet_peer_disconnect_now/*_later*/(peer,0);
    		/*while (true)
    		{
    			slCycle();
    			fpsStep();
    			while (enet_host_service(client,&event,0) > 0)
    			{
    				switch (event.type)
    				{
    					case ENET_EVENT_TYPE_RECEIVE:
    			*/		/*HandleEvent(event.channelID,event.packet);*/
    			/*		// I see no reason why we can't continue to handle packets even though we want to disconnect.
    					// EDIT: Actually, now I do. By this point we've already destroyed the UI.
    					enet_packet_destroy(event.packet);
    					//printf("Discarded packet.\n");
    					break;
    					case ENET_EVENT_TYPE_DISCONNECT:
    					//printf("Disconnected gracefully.\n");
    					goto CLEANUP;
    				};
    			};
    		};*/
    	//};
    	//printf("Disconnected forcefully.\n");
	}
	enet_host_destroy(client);
	enet_deinitialize();
};
