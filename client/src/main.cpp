/* CLIENT */
#include <slice.h>
#include <sliceopts.h>
#include <slicefps.h>
#include "online.h"
#include "local.h"
#include "mainmenu.h"
void ShowLicense_enet ()
{
	system("notepad LICENSE.enet.txt");
};
bool AboutBack;
void OnAboutBack () { AboutBack = true; };
void About ()
{
	AboutBack = false;
	slBox* button_enet = slCreateBox(slRenderText("ENet License"));
	slSetBoxDims(button_enet,0.3,0.475,0.4,0.05,30);
	button_enet->onclick = ShowLicense_enet;
	MainMenuButton(button_enet);
	slBox* backbutton = slCreateBox(slRenderText("Back"));
	slSetBoxDims(backbutton,0.4,0.55,0.2,0.05,30);
	backbutton->onclick = OnAboutBack;
	MainMenuButton(backbutton);
	while (!slGetReqt())
	{
		slCycle();
		if (AboutBack) break;
	};
	slDestroyBox(button_enet);
	slDestroyBox(backbutton);
};
Uint8 MainMenuSelection;
#define MainMenu_Online 1
#define MainMenu_Local 2
#define MainMenu_About 3
void OnOnlineSelect () { MainMenuSelection = MainMenu_Online; };
void OnLocalSelect () { MainMenuSelection = MainMenu_Local; };
void OnAboutSelect () { MainMenuSelection = MainMenu_About; };
void MainMenu ()
{
	MainMenuSelection = 0;
	slBox* splash = slCreateBox(slLoadTexture("c4/splash.png"));
	slSetBoxDims(splash,0.3,0.22,0.4,0.08,30);
	slBox* OnlineButton = slCreateBox(slRenderText("Connect to Public Lobby (Online)"));
	slSetBoxDims(OnlineButton,0.2,0.34,0.6,0.08,30);
	OnlineButton->onclick = OnOnlineSelect;
	MainMenuButton(OnlineButton);
	slBox* LocalButton = slCreateBox(slRenderText("Local 2-Player Game (Offline)"));
	slSetBoxDims(LocalButton,0.2,0.46,0.6,0.08,30);
	LocalButton->onclick = OnLocalSelect;
	MainMenuButton(LocalButton);
	slBox* QuitButton = slCreateBox(slRenderText("Exit"));
	slSetBoxDims(QuitButton,0.375,0.76,0.25,0.05,30);
	QuitButton->onclick = slSignalExit;
	MainMenuButton(QuitButton);
	slBox* OptsButton = slCreateBox(slRenderText("Options"));
	slSetBoxDims(OptsButton,0.375,0.67,0.25,0.05,30);
	OptsButton->onclick = opOpen;
	MainMenuButton(OptsButton);
	slBox* AboutButton = slCreateBox(slRenderText("About"));
	slSetBoxDims(AboutButton,0.375,0.58,0.25,0.05,30);
	AboutButton->onclick = OnAboutSelect;
	MainMenuButton(AboutButton);
	while (!slGetExitReq())
	{
		slCycle();
		if (MainMenuSelection) break;
	};
	slDestroyBox(OnlineButton);
	slDestroyBox(LocalButton);
	slDestroyBox(QuitButton);
	slDestroyBox(OptsButton);
	slDestroyBox(AboutButton);
	slDestroyBox(splash);
	switch (MainMenuSelection)
	{
		case MainMenu_Online: DoOnline(); break;
		case MainMenu_Local: DoLocal(); break;
		case MainMenu_About: About(); //break;
	};
};
extern char* Host;
int main (int argc, char** argv)
{
	Host = GetArg(argc,argv,"-host=");
	if (!Host) Host = "conn4.glassbladegames.com"; // Default to the official server. /*It's down these days, sadly.*/
	slInit("Connect Four");
	opInit();
	fpsInit();
	opSetExitVisible(false);
	while (!slGetExitReq()) MainMenu();
	fpsQuit();
	opQuit();
	slQuit();
};
