#include <slice.h>
#include <slicefps.h>
#include "local.h"
#include "mainmenu.h"
slBox** Droppers;
Uint8* states;
#define Red 1
#define Blue 2
#define Empty 0
bool IsBlue = false; // Insert racist joke here... except blue has been changed to black now.
bool CheckWin (Sint8 x, Sint8 y)
{
	Uint8 lookingfor = IsBlue ? Blue : Red;
	Uint8 i;
	Uint8 found;
	Sint8 a,b;
	/// Increasing Diagonal
	i = 0;
	a = x - 3;
	b = y - 3;
	while (a < 0 || b < 0)
	{
		a++;
		b++;
		i++;
	};
	found = 0;
	while (i < 7)
	{
		if (a > 6 || b > 5) break;
		if (*(states + (b * 7) + a) == lookingfor)
		{
			found++;
			if (found >= 4) return true;
		}
		else found = 0;
		a++;
		b++;
		i++;
	};
	/// Decreasing Diagonal
	i = 0;
	a = x - 3;
	b = y + 3;
	while (a < 0 || b > 5)
	{
		a++;
		b--;
		i++;
	};
	found = 0;
	while (i < 7)
	{
		if (a > 6 || b < 0) break;
		if (*(states + (b * 7) + a) == lookingfor)
		{
			found++;
			if (found >= 4) return true;
		}
		else found = 0;
		a++;
		b--;
		i++;
	};
	/// Vertical
	i = 0;
	b = y - 3;
	while (b < 0)
	{
		b++;
		i++;
	};
	found = 0;
	while (i < 7)
	{
		if (b > 5) break;
		if (*(states + (b * 7) + x) == lookingfor)
		{
			found++;
			if (found >= 4) return true;
		}
		else found = 0;
		b++;
		i++;
	};
	/// Horizontal
	i = 0;
	a = x - 3;
	b = y * 7;
	while (a < 0)
	{
		a++;
		i++;
	};
	found = 0;
	while (i < 7)
	{
		if (a > 6) break;
		if (*(states + b + a) == lookingfor)
		{
			found++;
			if (found >= 4) return true;
		}
		else found = 0;
		a++;
		i++;
	};
	/// Failure
	return false;
};
slList Coins("Coins",slNoIndex,false);
slBox* DroppingCoin = NULL;
slScalar DroppingCoinTargetY;
slBox* WinTextBox;
void ResetLocalGame ()
{
	WinTextBox->visible = false;
	memset(states,Empty,42);
	IsBlue = false;
	slBox* box;
	for (Uint8 x = 0; x < 7; x++)
	{
		box = *(Droppers + x);
		box->visible = true;
		box->SetHoverTexRef(slLoadTexture(IsBlue ? "c4/coin_blue.png" : "c4/coin_red.png"));
	};
	Coins.Clear(slDestroyBox);
	DroppingCoin = NULL;
};
slBU BlueScore = 0;
slBU RedScore = 0;
slBox* ScoreBox;
void UpdateScores ()
{
	char* scoretext;
	asprintf(&scoretext,"Blue: %u | Red: %u",(unsigned int)BlueScore,(unsigned int)RedScore);
	ScoreBox->SetTexRef(slRenderText(scoretext));
	free(scoretext);
};
void EndGame (bool stalemate = false)
{
	WinTextBox->visible = true;
	if (stalemate) WinTextBox->SetTexRef(slRenderText("Nobody Wins! (Click to replay.)"));
	else
	{
		if (IsBlue)
		{
			WinTextBox->SetTexRef(slRenderText("Blue Wins! (Click to replay.)"));
			BlueScore++;
		}
		else
		{
			WinTextBox->SetTexRef(slRenderText("Red Wins! (Click to replay.)"));
			RedScore++;
		};
		UpdateScores();
	};
};
void OnLocalDrop (slBox* dropper)
{
	if (DroppingCoin) return;
	Uint8 x;
	// Stop checking after 6-1, not 7-1, because 7-1 is logically going to be it.
	for (x = 0; x < 6; x++) if (dropper == *(Droppers + x)) break;
	for (Uint8 y = 0; y < 6; y++)
	{
		if (y == 5) dropper->visible = false;
		if (!*(states + (y * 7) + x))
		{
			*(states + (y * 7) + x) = IsBlue ? Blue : Red;
			slBox* coin = slCreateBox(slLoadTexture(IsBlue ? "c4/coin_blue.png" : "c4/coin_red.png"));
			slSetBoxDims(coin,0.15 + (x * 0.1),dropper->xy.y,0.1,0.1,128);
			DroppingCoinTargetY = 0.7 - (y * 0.1);
			DroppingCoin = coin;
			coin->maintainaspect = true;
			Coins.Add(coin);
			if (CheckWin(x,y)) EndGame();
			else
			{
				IsBlue = !IsBlue;
				for (x = 0; x < 7; x++) (*(Droppers + x))->SetHoverTexRef(slLoadTexture(IsBlue ? "c4/coin_blue.png" : "c4/coin_red.png"));
			};
			return;
		};
	};
};
slBox** slots;
slBox** sides;
slBox* TopLeftCorner;
slBox* TopRightCorner;
slBox* BottomLeftCorner;
slBox* BottomRightCorner;
void CreateBoard ()
{
	Uint8 x,y;
	slots = malloc(sizeof(slBox*) * 42);
	for (x = 0; x < 7; x++) for (y = 0; y < 6; y++)
	{
		slBox* slot = slCreateBox(slLoadTexture("c4/square.png"));
		slSetBoxDims(slot,0.15 + (x * 0.1),0.2 + (y * 0.1),0.1,0.1,64);
		slot->maintainaspect = true;
//		slot->gapcolor = {0xe1,0xb2,0x34,0xff};
		slot->bordercolor = {0xee,0xc8,0x59,0xff};//{0xde,0xa3,0x03,0xff};
		*(slots + (y * 7) + x) = slot;
	};
	sides = malloc(sizeof(slBox*) * 14);
	for (y = 0; y < 6; y++)
	{
		slBox* left = slCreateBox(slLoadTexture("c4/leftside.png"));
		slSetBoxDims(left,0.05,0.2 + (y * 0.1),0.1,0.1,64);
		left->maintainaspect = true;
		*(sides++) = left;
		slBox* right = slCreateBox(slLoadTexture("c4/rightside.png"));
		slSetBoxDims(right,0.85,0.2 + (y * 0.1),0.1,0.1,64);
		right->maintainaspect = true;
		*(sides++) = right;
	};
	sides -= 12;
	TopLeftCorner = slCreateBox(slLoadTexture("c4/topleft.png"));
	slSetBoxDims(TopLeftCorner,0.05,0.2,0.1,0.1,64);
	TopLeftCorner->maintainaspect = true;
	TopRightCorner = slCreateBox(slLoadTexture("c4/topright.png"));
	slSetBoxDims(TopRightCorner,0.85,0.2,0.1,0.1,64);
	TopRightCorner->maintainaspect = true;
	BottomLeftCorner = slCreateBox(slLoadTexture("c4/bottomleft.png"));
	slSetBoxDims(BottomLeftCorner,0.05,0.7,0.1,0.1,64);
	BottomLeftCorner->maintainaspect = true;
	BottomRightCorner = slCreateBox(slLoadTexture("c4/bottomright.png"));
	slSetBoxDims(BottomRightCorner,0.85,0.7,0.1,0.1,64);
	BottomRightCorner->maintainaspect = true;
};
void DestroyBoard ()
{
	Uint8 x;
	for (x = 0; x < 42; x++) slDestroyBox(*(slots + x));
	free(slots);
	for (x = 0; x < 12; x++) slDestroyBox(*(sides + x));
	free(sides);
	slDestroyBox(TopLeftCorner);
	slDestroyBox(TopRightCorner);
	slDestroyBox(BottomLeftCorner);
	slDestroyBox(BottomRightCorner);
};
void SetBoardVis (bool vis)
{
	Uint8 x;
	for (x = 0; x < 42; x++) ((slBox*)*(slots + x))->visible = vis;
	for (x = 0; x < 12; x++) ((slBox*)*(sides + x))->visible = vis;
	TopLeftCorner->visible = vis;
	TopRightCorner->visible = vis;
	BottomLeftCorner->visible = vis;
	BottomRightCorner->visible = vis;
};
bool LocalQuitReq;
void OnLocalQuit () { LocalQuitReq = true; };
void DoLocal ()
{
	ScoreBox = slCreateBox();
	slSetBoxDims(ScoreBox,0,0.9,1,0.05,40);
	UpdateScores();
	WinTextBox = slCreateBox();
	slSetBoxDims(WinTextBox,0,0,1,1,29);
	WinTextBox->onclick = ResetLocalGame;
	WinTextBox->visible = false;
	states = calloc(42,1); // 7 by 6
	Droppers = malloc(sizeof(slBox*) * 7);
	CreateBoard();
	Uint8 x,y;
	for (x = 0; x < 7; x++)
	{
		slBox* dropper = slCreateBox(slLoadTexture("c4/arrow.png"),slLoadTexture("c4/coin_red.png"));
		slSetBoxDims(dropper,0.15 + (x * 0.1),0.1,0.1,0.1,96);
		dropper->maintainaspect = true;
		dropper->hoverable = true;
		dropper->onclick = OnLocalDrop;
		*(Droppers + x) = dropper;
	};
	slBox* LocalQuitButton = slCreateBox(slRenderText("Back to Main Menu"));
	slSetBoxDims(LocalQuitButton,0.01,0.94,0.23,0.05,28);
	MainMenuButton(LocalQuitButton);
	LocalQuitButton->onclick = OnLocalQuit;
	LocalQuitReq = false;
	while (!slGetExitReq())
	{
		slCycle();
		if (LocalQuitReq) break;
		if (DroppingCoin)
		{
			DroppingCoin->xy.y += slGetDelta() * 4;
			if (DroppingCoin->xy.y >= DroppingCoinTargetY)
			{
				DroppingCoin->xy.y = DroppingCoinTargetY;
				DroppingCoin = NULL;
			};
		};
		for (x = 0; x < 7; x++) if ((*(Droppers + x))->visible) goto OK;
		// Stalemate!
		EndGame(true);
		OK:;
	};
	/* Clean up the UI. */
	slDestroyBox(LocalQuitButton);
	slDestroyBox(WinTextBox);
	slDestroyBox(ScoreBox);
	free(states);
	DestroyBoard();
	for (x = 0; x < 7; x++) slDestroyBox(*(Droppers + x));
	free(Droppers);
	Coins.Clear(slDestroyBox);
	DroppingCoin = NULL;
};
