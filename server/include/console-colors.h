#pragma once
#include "stdint.h"
#define CC_BLACK 0
#define CC_DARKBLUE 1
#define CC_DARKGREEN 2
#define CC_DARKCYAN 3
#define CC_DARKRED 4
#define CC_DARKPINK 5
#define CC_DARKYELLOW 6
#define CC_DARKWHITE 7
#define CC_GRAY 8
#define CC_BLUE 9
#define CC_GREEN 10
#define CC_CYAN 11
#define CC_RED 12
#define CC_PURPLE 13
#define CC_YELLOW 14
#define CC_WHITE 15
void SetNextColor (uint8_t textcolor = CC_WHITE, uint8_t backcolor = CC_BLACK);
/*
#define CC_BLACK '0'
#define CC_BLUE '1'
#define CC_GREEN '2'
#define CC_AQUA '3'
#define CC_RED '4'
#define CC_PURPLE '5'
#define CC_YELLOW '6'
#define CC_WHITE '7'
#define CC_GRAY '8'
#define CC_LIGHT_BLUE '9'
#define CC_LIGHT_GREEN 'A'
#define CC_LIGHT_AQUA 'B'
#define CC_LIGHT_RED 'C'
#define CC_LIGHT_PURPLE 'D'
#define CC_LIGHT_YELLOW 'E'
#define CC_BRIGHT_WHITE 'F'
*/
