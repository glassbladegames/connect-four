/* SERVER */
#define conn4_port 42069
#include <enet/enet.h>
#include <slice.h>
#include "console-colors.h"
#include "../event-ids.h"
Uint32 next_user_id = 0;
struct Client
{
	Uint32 user_id;
	ENetPeer* peer;
	char* name;
	slList invites;
	time_t joined_time;
	slBU _index_;
};
slList Clients = slListInit("Clients",offsetof(Client,_index_),false,8);
struct Game
{
	Client* red;
	Client* blue;
	Uint8 states [42];
	bool is_blue; // Red 1 Blue 0
	slBU _index_;
};
slList Games = slListInit("Games",offsetof(Game,_index_),false,4);
bool CheckWin (Game* game, Sint8 x, Sint8 y)
{
	Uint8 lookingfor = game->is_blue ? Blue : Red;
	Uint8 i;
	Uint8 found;
	Sint8 a,b;
	/// Increasing Diagonal
	i = 0;
	a = x - 3;
	b = y - 3;
	while (a < 0 || b < 0)
	{
		a++;
		b++;
		i++;
	};
	found = 0;
	while (i < 7)
	{
		if (a > 6 || b > 5) break;
		if (*(game->states + (b * 7) + a) == lookingfor)
		{
			found++;
			if (found >= 4) return true;
		}
		else found = 0;
		a++;
		b++;
		i++;
	};
	/// Decreasing Diagonal
	i = 0;
	a = x - 3;
	b = y + 3;
	while (a < 0 || b > 5)
	{
		a++;
		b--;
		i++;
	};
	found = 0;
	while (i < 7)
	{
		if (a > 6 || b < 0) break;
		if (*(game->states + (b * 7) + a) == lookingfor)
		{
			found++;
			if (found >= 4) return true;
		}
		else found = 0;
		a++;
		b--;
		i++;
	};
	/// Vertical
	i = 0;
	b = y - 3;
	while (b < 0)
	{
		b++;
		i++;
	};
	found = 0;
	while (i < 7)
	{
		if (b > 5) break;
		if (*(game->states + (b * 7) + x) == lookingfor)
		{
			found++;
			if (found >= 4) return true;
		}
		else found = 0;
		b++;
		i++;
	};
	/// Horizontal
	i = 0;
	a = x - 3;
	b = y * 7;
	while (a < 0)
	{
		a++;
		i++;
	};
	found = 0;
	while (i < 7)
	{
		if (a > 6) break;
		if (*(game->states + b + a) == lookingfor)
		{
			found++;
			if (found >= 4) return true;
		}
		else found = 0;
		a++;
		i++;
	};
	/// Failure
	return false;
};
Game* GetClientGame (Client* client)
{
	for (slBU cur = 0; cur < Games.itemcount; cur++)
	{
		Game* game = *(Games.items + cur);
		if (game->red == client || game->blue == client) return game;
	};
	return NULL;
};
slBU GamesPlayed = 0;
slBU PeakGames = 0;
void EndGame (Game* game, Uint8 status)
{
	SetNextColor(CC_GREEN);
	printf("Game has ended between clients [#%u] and [#%u].\n",(unsigned int)game->blue->user_id,(unsigned int)game->red->user_id);

	Uint8 buf [6];
	*buf = UserEvent_PlayerStatus;
	buf[5] = 0;
	ENetPacket* packet;
	/* Tell other users these players are no longer in-game. */
	for (slBU cur = 0; cur < Clients.itemcount; cur++)
	{
		Client* client = *(Clients.items + cur);
		if (client != game->blue)
		{
			*(Uint32*)(buf + 1) = game->blue->user_id;
			packet = enet_packet_create(buf,6,ENET_PACKET_FLAG_RELIABLE);
			enet_peer_send(client->peer,0,packet);
		};
		if (client != game->red)
		{
			*(Uint32*)(buf + 1) = game->red->user_id;
			packet = enet_packet_create(buf,6,ENET_PACKET_FLAG_RELIABLE);
			enet_peer_send(client->peer,0,packet);
		};
	};
	/* Tell the users themselves the game is over. */
	*buf = GameEvent_GameOver;
	buf[1] = status;
	packet = enet_packet_create(&buf,2,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(game->red->peer,0,packet);
	packet = enet_packet_create(&buf,2,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(game->blue->peer,0,packet);
	slListRemove(&Games,game);
	//free(game->states);
	free(game);

	SetNextColor(CC_WHITE);
	printf("Games Active: %u (Peak Games: %u | Total Started: %u)\n",
		(unsigned int)Games.itemcount,
		(unsigned int)PeakGames,
		(unsigned int)GamesPlayed
	);
};
void OnGameAction (Client* client, ENetPacket* packet)
{
	Game* game = GetClientGame(client);
	if (!game) return; // The client isn't in a game.
	bool client_blue = client == game->blue;
	if (client_blue != game->is_blue) return; // It's not this client's turn.
	if (packet->dataLength < 2) return; // The packet is invalid.
	Uint8 x = *(packet->data + 1);
	/* Check the column. */
	for (Uint8 y = 0; y < 6; y++)
	{
		if (!*(game->states + (y * 7) + x))
		{
			*(game->states + (y * 7) + x) = client_blue ? Blue : Red;
			Uint8 buf [4] = {GameEvent_PiecePlaced,x,y,client_blue ? 1 : 0};
			ENetPacket* packet = enet_packet_create(buf,4,ENET_PACKET_FLAG_RELIABLE);
			enet_peer_send(game->blue->peer,0,packet);
			packet = enet_packet_create(buf,4,ENET_PACKET_FLAG_RELIABLE);
			enet_peer_send(game->red->peer,0,packet);
			if (CheckWin(game,x,y)) EndGame(game,client_blue ? GAMEOVER_BLUEWIN : GAMEOVER_REDWIN);
			else
			{
				game->is_blue = !client_blue;
				*buf = GameEvent_WhoseTurn;
				buf[1] = game->is_blue ? 1 : 0;
				packet = enet_packet_create(buf,2,ENET_PACKET_FLAG_RELIABLE);
				enet_peer_send(game->blue->peer,0,packet);
				packet = enet_packet_create(buf,2,ENET_PACKET_FLAG_RELIABLE);
				enet_peer_send(game->red->peer,0,packet);
				for (x = 35; x < 42; x++) if (!*(game->states + x)) return;
				/* Stalemate */
				*buf = GameEvent_GameOver;
				buf[1] = GAMEOVER_STALEMATE;
				packet = enet_packet_create(buf,2,ENET_PACKET_FLAG_RELIABLE);
				enet_peer_send(game->blue->peer,0,packet);
				packet = enet_packet_create(buf,2,ENET_PACKET_FLAG_RELIABLE);
				enet_peer_send(game->red->peer,0,packet);
			};
			return;
		};
	};
};
/*void  TellClientHisID (Client* client)
{
	Uint8 buf [5];
	*buf = UserEvent_TellID;
	*(Uint32*)(buf + 1) = client->user_id;
	ENetPacket* packet = enet_packet_create(buf,5,ENET_PACKET_FLAG_RELIABLE);
};*/
void SendNewPlayerEvent (Client* newguy, Client* receiver)
{
	Uint8 buf [5];
	*buf = UserEvent_NewPlayer;
	*(Uint32*)(buf + 1) = newguy->user_id;
	ENetPacket* packet = enet_packet_create(buf,5,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(receiver->peer,0,packet);
};
slBU PeakPlayers = 0;
void CreateClient (ENetPeer* peer)
{
	Client* out = malloc(sizeof(Client));
	out->peer = peer;
	out->name = NULL;
	next_user_id++; // Start users at #1, not #0.
	out->user_id = next_user_id;
	/**asprintf(&(out->name),"User %u",(unsigned int)out->user_id);**/
	/// Doesn't matter what the server thinks at this point, if the clients assume it on their own.
	/// Therefore, conserve this memory.
	// Give the user a default name based on his User ID.
	out->invites = slListInit("Client's invites",slNoIndex,false,1);
	out->joined_time = time(NULL);
	peer->data = out;
	//TellClientHisID(out);
	/* Send client every other player's join event and name change event. */
	/* Also notify other clients this player has joined. */
	/**slBU defaultnamelen = slStrLen(out->name);
	Uint8* defaultnamebuf = malloc(defaultnamelen + 5);
	*defaultnamebuf = UserEvent_SetName;
	*(Uint32*)(defaultnamebuf + 1) = out->user_id;
	memcpy(defaultnamebuf + 5,out->name,defaultnamelen);**/
	/// If clients assume this before a name change occurs, network bandwidth is conserved!
	for (slBU cur = 0; cur < Clients.itemcount; cur++)
	{
		Client* other = *(Clients.items + cur);
		// Joins
		SendNewPlayerEvent(other,out);
		SendNewPlayerEvent(out,other);
		// Names
		// Other clients' names to new user.
		// Don't waste bandwidth sending a NULL name.
		if (other->name)
		{
			slBU len = slStrLen(other->name);
			Uint8* buf = malloc(len + 5);
			*buf = UserEvent_SetName;
			*(Uint32*)(buf + 1) = other->user_id;
			memcpy(buf + 5,other->name,len);
			ENetPacket* packet = enet_packet_create(buf,len + 5,ENET_PACKET_FLAG_RELIABLE);
			free(buf);
			enet_peer_send(peer,0,packet);
		};
		// New user's default name to other clients.
		/**ENetPacket* namepacket = enet_packet_create(defaultnamebuf,defaultnamelen + 5,ENET_PACKET_FLAG_RELIABLE);
		enet_peer_send(other->peer,0,namepacket);**/
	};
	/**free(defaultnamebuf);**/
	slListAdd(&Clients,out);
	/* Server Log */
		char* addrstr = malloc(16);
		enet_address_get_host_ip(&peer->address,addrstr,16);
		*(addrstr + 15) = '\0';
		//SetConsoleTextColor(CC_LIGHT_YELLOW);
		SetNextColor(CC_YELLOW);
		printf("Client connected from [%s]. User ID: [#%u]\n",addrstr,out->user_id);
		if (PeakPlayers < Clients.itemcount) PeakPlayers = Clients.itemcount;
		SetNextColor(CC_WHITE);
		printf("Players Online: %u (Peak Players: %u | Total Joins: %u)\n",
			(unsigned int)Clients.itemcount,
			(unsigned int)PeakPlayers,
			(unsigned int)next_user_id
		);
};
char* ElapsedTimeStr (time_t elapsed)
{
	unsigned int seconds = elapsed % 60;
	elapsed /= 60;
	unsigned int minutes = elapsed % 60;
	elapsed /= 60;
	unsigned int hours = elapsed % 24;
	elapsed /= 24;
	unsigned int days = elapsed;
	char* days_str;
	char* hms_str;
	if (days) asprintf(&days_str,"%u Days + ",days);
	else days_str = NULL;
	asprintf(&hms_str,"%u:%.2u:%.2u",hours,minutes,seconds);
	char* out = slStrConcat(days_str,hms_str);
	if (days_str) free(days_str);
	free(hms_str);
	return out;
};
void SendPlayerLeftEvent (Client* left, Client* receiver)
{
	Uint8 buf [5];
	*buf = UserEvent_PlayerLeft;
	*(Uint32*)(buf + 1) = left->user_id;
	ENetPacket* packet = enet_packet_create(buf,5,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(receiver->peer,0,packet);
};
slScalar avgtime;
slBU timecount = 0;
void DestroyClient (ENetPeer* peer)
{
	Client* todel = peer->data;
	/* Server Log */
		//SetConsoleTextColor(CC_LIGHT_YELLOW);
		SetNextColor(CC_YELLOW);
		printf("Client disconnected. User ID: [#%u]\n",todel->user_id);
		time_t thistime = time(NULL) - todel->joined_time;
		char* timestr = ElapsedTimeStr(thistime);
		timecount++;
		if (timecount) avgtime = ((avgtime * (timecount - 1)) + thistime) / timecount;
		else avgtime = thistime;
		char* avgtimestr = ElapsedTimeStr(avgtime + 0.5);
		SetNextColor(CC_DARKYELLOW);
		printf("Time Elapsed: %s (Average: %s)\n",timestr,avgtimestr);
		SetNextColor(CC_WHITE);
		printf("Players Online: %u (Peak Players: %u | Total Joins: %u)\n",
			(unsigned int)(Clients.itemcount - 1),
			(unsigned int)PeakPlayers,
			(unsigned int)next_user_id
		);
		free(timestr);
	Game* game = GetClientGame(todel);
	if (game) EndGame(game,GAMEOVER_ABANDON);
	slListRemove(&Clients,todel);
	/* Remove any invites made by this user. */
	/* Also notify all other clients this player has left. */
	for (slBU cur = 0; cur < Clients.itemcount; cur++)
	{
		Client* other = *(Clients.items + cur);
		/* Invites */
		for (slBU sub = 0; sub < other->invites.itemcount; sub++)
		{
			Client* invite = *(other->invites.items + sub);
			if (invite == todel)
			{
				slListRemove(&other->invites,invite);
				goto NEXT;
			};
		};
		NEXT:
		/* Notifying other players. */
		SendPlayerLeftEvent(todel,other);
	};
	/* Remove invites made by other users. */
	slListClear(&todel->invites,NULL);
	if (todel->name) free(todel->name);
	free(todel);
};
void Handle_UserEvent_SetName (Client* client, ENetPacket* packet)
{
	SetNextColor(CC_CYAN);
	printf("Client [#%u] changed name\n",(unsigned int)client->user_id);
	printf("\tfrom [%c%s%c]\n",client->name ? '"' : '<',client->name ? client->name : "default",client->name ? '"' : '>');
	//SetConsoleTextColor(CC_WHITE);
	//printf("Received name change message.\n");
	/* Set the client's new name. */
	char* name;
	size_t namelen = packet->dataLength - 1;
	if (namelen)
	{
		if (namelen > 80) namelen = 80;
		name = malloc(namelen + 1);
		*(name + namelen) = '\0';
		memcpy(name,packet->data + 1,namelen);
	}
	else name = NULL;
	if (client->name) free(client->name);
	client->name = name;
	/* Tell the other clients about this one's name change. */
	/* The originating player does not need a response. */
	Uint8* buf = malloc(namelen + 5);
	*buf = UserEvent_SetName;
	*(Uint32*)(buf + 1) = client->user_id;
	if (name) memcpy(buf + 5,name,namelen);
	for (slBU cur = 0; cur < Clients.itemcount; cur++)
	{
		Client* otherclient = *(Clients.items + cur);
		if (otherclient == client) continue;
		ENetPacket* outbound = enet_packet_create(buf,namelen + 5,ENET_PACKET_FLAG_RELIABLE);
		enet_peer_send(otherclient->peer,0,outbound);
	};
	free(buf);
	//printf("Name change replicated.\n");
	printf("\tto [%c%s%c].\n",client->name ? '"' : '<',client->name ? client->name : "default",client->name ? '"' : '>');
};
Client* ClientByID (Uint32 id)
{
	for (slBU cur = 0; cur < Clients.itemcount; cur++)
	{
		Client* client = *(Clients.items + cur);
		if (client->user_id == id) return client;
	};
	return NULL;
};
void Handle_UserEvent_InvitePlayer (Client* client, ENetPacket* packet)
{
	//SetConsoleTextColor(CC_WHITE);
	//printf("Received invite message.\n");
	if (packet->dataLength < 5) return;
	Uint32 recipient_id = *(Uint32*)(packet->data + 1);
	if (client->user_id == recipient_id) return; // Don't let users invite themselves.
	Client* otherclient = ClientByID(recipient_id);
	if (!otherclient)
	{
		SetNextColor(CC_RED);
		printf("Client [#%u] send an invalid invite!\n",(unsigned int)client->user_id);
		return;
	};
	FOUND_RECIPIENT:
	slListAdd(&client->invites,otherclient);
	Uint8 buf [5];
	*buf = UserEvent_InvitePlayer;
	*(Uint32*)(buf + 1) = client->user_id;
	ENetPacket* outbound = enet_packet_create(buf,5,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(otherclient->peer,0,outbound);
	SetNextColor(CC_DARKGREEN);
	printf("Client [#%u] invited client [#%u].\n",(unsigned int)client->user_id,recipient_id);
};
void StartGame (Client* a, Client* b)
{
	SetNextColor(CC_GREEN);
	printf("Starting game between [#%u] and [#%u].\n",(unsigned int)a->user_id,(unsigned int)b->user_id);
	Game* out = malloc(sizeof(Game));
	out->is_blue = false;
	//out->states = calloc(42,1);
	memset(out->states,0,42);
	Uint8 buf [6];
	*buf = GameEvent_GameStart;
	if (rand() % 2)
	{
		out->blue = a;
		out->red = b;
		buf[1] = 1;
	}
	else
	{
		out->blue = b;
		out->red = a;
		buf[1] = 0;
	};
	slListAdd(&Games,out);
	/* Tell these players they are in a game together. */
	slListClear(&a->invites,NULL);
	slListClear(&b->invites,NULL);
	*(Uint32*)(buf + 2) = b->user_id;
	ENetPacket* packet = enet_packet_create(buf,6,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(a->peer,0,packet);
	buf[1] = buf[1] ? 0 : 1;
	*(Uint32*)(buf + 2) = a->user_id;
	packet = enet_packet_create(buf,6,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(b->peer,0,packet);
	/* Tell the clients it's red's turn. */
	*buf = GameEvent_WhoseTurn;
	buf[1] = 0;
	packet = enet_packet_create(buf,2,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(a->peer,0,packet);
	packet = enet_packet_create(buf,2,ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(b->peer,0,packet);
	/* Notify all other clients that these players are in a game. */
	*buf = UserEvent_PlayerStatus;
	buf[5] = 1;
	for (slBU cur = 0; cur < Clients.itemcount; cur++)
	{
		Client* client = *(Clients.items + cur);
		if (client != a && client != b)
		{
			*(Uint32*)(buf + 1) = a->user_id;
			packet = enet_packet_create(buf,6,ENET_PACKET_FLAG_RELIABLE);
			enet_peer_send(client->peer,0,packet);
			*(Uint32*)(buf + 1) = b->user_id;
			packet = enet_packet_create(buf,6,ENET_PACKET_FLAG_RELIABLE);
			enet_peer_send(client->peer,0,packet);
		};
	};

	GamesPlayed++;
	SetNextColor(CC_WHITE);
	if (PeakGames < Games.itemcount) PeakGames = Games.itemcount;
	printf("Games Active: %u (Peak Games: %u | Total Started: %u) \n",
		(unsigned int)Games.itemcount,
		(unsigned int)PeakGames,
		(unsigned int)GamesPlayed
	);
};
void Handle_UserEvent_AcceptInvite (Client* client, ENetPacket* packet)
{
	if (packet->dataLength < 5) return;
	//printf("Game invite accepted.\n");
	Uint32 inviter_id = *(Uint32*)(packet->data + 1);
	Client* inviter = ClientByID(inviter_id);
	if (!inviter) return;
	for (slBU cur = 0; cur < inviter->invites.itemcount; cur++) if (*(inviter->invites.items + cur) == client) goto LEGIT_INVITE;
	return;
	LEGIT_INVITE:
	if (GetClientGame(client) || GetClientGame(inviter))
	//{
		//printf("User already in a game.\n");
		return;
	//};
	StartGame(client,inviter);
};
void HandleClient (ENetPeer* peer, enet_uint8 channel, ENetPacket* packet)
{
	Client* client = peer->data;
	//switch (channel)
	//{
		//case 0:
		if (!packet->dataLength) return;
		switch (*(packet->data))
		{
			case UserEvent_SetName:
			Handle_UserEvent_SetName(client,packet);
			break;
			case UserEvent_InvitePlayer:
			Handle_UserEvent_InvitePlayer(client,packet);
			break;
			case UserEvent_AcceptInvite:
			Handle_UserEvent_AcceptInvite(client,packet);
			break;
			case GameEvent_PiecePlaced:
			OnGameAction(client,packet);
			//break;
		};
		//break;
	//};
};
int main ()
{
	if (enet_initialize())
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"Could not initialize ENet.","Server failed to start.",NULL);
		exit(-1);
	};
	ENetAddress addr;
	addr.host = ENET_HOST_ANY;
	addr.port = conn4_port;
	ENetHost* host = enet_host_create(&addr,1024,4,0,0);
	if (!host) slFatal("Port already occupied.",conn4_port);
	while (true)
	{
		Sleep(50);
		ENetEvent event;
		while (enet_host_service(host,&event,0) > 0)
		{
			switch (event.type)
			{
				case ENET_EVENT_TYPE_CONNECT:
				CreateClient(event.peer);
				break;
				case ENET_EVENT_TYPE_DISCONNECT:
				DestroyClient(event.peer);
				break;
				case ENET_EVENT_TYPE_RECEIVE:
				HandleClient(event.peer,event.channelID,event.packet);
				enet_packet_destroy(event.packet);
			};
		};
	};
	enet_host_destroy(host);
	enet_deinitialize();
	return 0;
};
