#include "console-colors.h"
#include "windows.h"
void SetNextColor (uint8_t textcolor = CC_WHITE, uint8_t backcolor = CC_BLACK)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),(backcolor << 4) | textcolor);
};
/*
char TextColorID = CC_WHITE;
char BackColorID = CC_BLACK;
void UpdateConsoleColors ()
{
	char cmdstr [9] = "COLOR --";
	cmdstr[6] = BackColorID;
	cmdstr[7] = TextColorID;
	system(cmdstr);
};
void SetConsoleTextColor (char id)
{
	TextColorID = id;
	UpdateConsoleColors();
};
void SetConsoleBackColor (char id)
{
	BackColorID = id;
	UpdateConsoleColors();
};
*/
