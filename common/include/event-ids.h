#pragma once

#define UserEvent_SetName 0
/* {Uint32 len, char* name} */

#define UserEvent_InvitePlayer 1
/* TO SERVER: {Uint32 invitee_id} */
/* TO CLIENT: {Uint32 inviter_id} */

#define UserEvent_AcceptInvite 2
/* {Uint32 inviter_id} */

//#define UserEvent_TellID 3

#define UserEvent_NewPlayer 4
/* {Uint32 player_id} */

#define UserEvent_PlayerLeft 5
/* {Uint32 player_id} */

#define GameEvent_GameOver 100
/* {Uint8 status} */
#define GAMEOVER_ABANDON 0
#define GAMEOVER_REDWIN 1
#define GAMEOVER_BLUEWIN 2
#define GAMEOVER_STALEMATE 3

#define GameEvent_WhoseTurn 101
/* {Uint8 player_is_black ? 1 : 0} */

#define GameEvent_PiecePlaced 102
/* {Uint8 x, Uint8 y, Uint8 piece_is_black ? 1 : 0} */

#define GameEvent_GameStart 103
/* {Uint8 i_am_black ? 1 : 0, Uint32 other_player_id} */

#define UserEvent_PlayerStatus 6
/* {Uint32 id, Uint8 ingame ? 1 : 0} */

#define Red 1
#define Blue 2
#define Empty 0


